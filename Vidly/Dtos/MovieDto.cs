﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Vidly.Dtos
{
    public class MovieDto
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(255)]
        public string Name { get; set; }

        public DateTime DateAdded { get; set; }

        public DateTime ReleaseDate { get; set; }

        [Range(0, 100)]
        public byte NumberInStock { get; set; }

        public GenreDto Genre { get; set; }
        [Required]
        public byte GenreId { get; set; }
    }
}