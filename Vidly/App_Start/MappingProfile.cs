﻿using AutoMapper;
using Vidly.Dtos;
using Vidly.Models;

namespace Vidly.App_Start
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            Mapper.CreateMap<Customer, CustomerDto>();
            Mapper.CreateMap<CustomerDto, Customer>().ForMember(e => e.Id, x => x.Ignore());

            Mapper.CreateMap<Movie, MovieDto>();
            Mapper.CreateMap<MovieDto, Movie>().ForMember(e => e.Id, x => x.Ignore());

            Mapper.CreateMap<Genre, GenreDto>();

            Mapper.CreateMap<MembershipType, MembershipTypeDto>();
        }
    }
}