namespace Vidly.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class SeedUsers : DbMigration
    {
        public override void Up()
        {

            Sql(@"
                    INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'aa7edb62-95d5-46d6-a451-34add6bd45d3', N'CanManageMovies')
                ");

            Sql(@"
                    INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'0780b742-831c-4d30-898b-15a4030426de', N'guest@gmail.com', 0, N'AAyYQk8yGvgw8buApUya7pDOVXTSCQULZQ7dDbpkZxYGYkzsi6X4xzk0ASEluVa8eQ==', N'e6c5e1aa-64ad-4900-b02e-36b519a8ee78', NULL, 0, 0, NULL, 1, 0, N'guest@gmail.com')
                    INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'955d1c1e-c594-425c-9ba9-d8593fec8fbd', N'admin@gmail.com', 0, N'AIVNaT9tFDoMcixan0S+XN+Kl6yssfP1bmPddA81fTDAh5ehUrBECTDWmIFqgwh3dg==', N'42ae5653-94d4-41a1-a709-979837a62335', NULL, 0, 0, NULL, 1, 0, N'admin@gmail.com')
                ");

            Sql(@"
                    INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'955d1c1e-c594-425c-9ba9-d8593fec8fbd', N'aa7edb62-95d5-46d6-a451-34add6bd45d3')
                ");

        }

        public override void Down()
        {
        }
    }
}
