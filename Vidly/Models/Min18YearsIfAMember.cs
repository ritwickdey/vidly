﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Vidly.Models
{
    public class Min18YearsIfAMember : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var customer = validationContext.ObjectInstance as Customer;

            if (customer.MembershipTypeId == MembershipType.Unknown
                || customer.MembershipTypeId == MembershipType.PayAsYouGo)
            {
                return ValidationResult.Success;
            }

            if (!customer.Birthdate.HasValue)
            {
                return new ValidationResult(
                    "Birthday is requried other than 'Pay As You Go' Membership.");
            }

            var age = DateTime.Today.Year - customer.Birthdate.Value.Year;

            return age >= 18
                ? ValidationResult.Success
                : new ValidationResult(
                    "Customer should be at least 18 years old to go with other than 'Pay As You Go' membership.");

        }
    }
}