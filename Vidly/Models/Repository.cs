﻿using System.Collections.Generic;

namespace Vidly.Models
{
    public class LocalRepository
    {
        public List<Customer> GetAllCustomer()
        {
            return new List<Customer>()
            {
                new Customer()
                {
                    Id = 0,
                    Name = "Ritwick Dey"
                },
                new Customer()
                {
                    Id = 1,
                    Name = "Akaksh"
                },
                new Customer()
                {
                    Id = 3,
                    Name = "Rahul Pandit"
                }
            };

        }

        public List<Movie> GetAllMovies()
        {
            return new List<Movie>()
            {
                new Movie()
                {
                    Id = 0,
                    Name = "Movie Name 1"
                },
                new Movie()
                {
                    Id = 1,
                    Name = "Movie Name 2"
                }
            };
        }
    }
}